$(document).ready(function(){
    $('#search').keyup(function(){
        var searchValue = ($('#search').val()).trim();
        if(searchValue != '') {
            $.getJSON('plugins/buscador/list.json', function(result){
                var list = result.list;
                var options = {
                    shouldSort: true,
                    threshold: 0.6,
                    location: 0,
                    distance: 100,
                    masPatternLength: 31,
                    minMatchCharLength: 1,
                    keys: [
                        "Doctors",
                        "especialidad",
                        
                    ]
                };
                var fuse = new Fuse(list, options);
                var searchResult = fuse.search(searchValue);
                
                if(searchResult.length > 0) {
                    $('#results').empty();
                    for(i = 0; i < searchResult.length; i++) {
                        $('#results').append('<a href="'+searchResult[i].Links+'"><div class="res"><h3 >'+searchResult[i].Doctors+'<p>'+searchResult[i].especialidad+'</p></h3></a><div>')
                    }
                }
                else{
                    $('#results').empty();
                    $('#results').append('<div class="res d-block"><p>Sin Resultados</p></div>')
                }

            })
        }
    })
})